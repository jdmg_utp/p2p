#include <czmq.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>


using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::pair;

using PairIpSk=pair<string,string>;//{ip,puerto}

string intToS(int n)
{
  std::ostringstream sin;
  sin << n;
  std::string val = sin.str();
  return val;
}

class peer{
private:
   zframe_t* identity;
   string ip;
   string port;
   string name_peer;
   vector<PairIpSk> table_to_send;
   
   
public:

   peer(zframe_t* _identity){
     identity=_identity;
     ip="localhost";
     zframe_print(identity,"identity");  
   }
   
   peer(string _ip,zframe_t* _identity){
     identity=_identity;
     ip= _ip;
     zframe_print(identity,"identity");  
   }
   
    peer(zframe_t* _identity,string _ip,string _port){
     identity=_identity;
     port=_port;
     ip= _ip;
     zframe_print(identity,"identity");  
   }
   
   peer(string _port){
     port=_port;
     ip= "localhost";
     //cout<<
   }
   
   void setName_peer(string _name_peer){
     name_peer=_name_peer;
   }

   
   void setNewSocket(string _ip,string _port){
       table_to_send.push_back({_ip,_port});
   }
   
   vector<PairIpSk> getSockets(){
     return table_to_send;
   }
   
  void sendMsg(zsock_t* channel, vector<string> parts) {
      zframe_t* cloneId= zframe_dup(identity);
	  zmsg_t* msg = zmsg_new();
	  zmsg_prepend(msg, &cloneId);
	  for (const string& s : parts) {
		 zmsg_addstr(msg, s.c_str());
	  }
	  zmsg_send(&msg, channel);
  }
  
 void sendMsg(zsock_t* channel, string action, vector<zframe_t*> identities) {
	  zmsg_t* msg = zmsg_new();
	  zframe_t* cloneId= zframe_dup(identity);
	  zmsg_append(msg, &cloneId);
	  zmsg_addstr(msg, action.c_str());
	  for (zframe_t* &frm : identities) {
		 zmsg_push(msg,frm);
	  }
	  zmsg_send(&msg, channel);
 }
  
  
  zframe_t* getZframe(){
   // zframe_print(identity);
    return zframe_dup(identity);
  }
  
  string getIp(){
      return ip;
  }
  
  string getPort(){
     return port;
  }
};


class NodeState {

public:

  zframe_t* My_Id;
  vector<peer> peers;
  int N; //Numero de nodos
  int GUID;
  int max_N; //Numero de nodos maximo
  pair <int,int> available_ports={1024,49151};
  
  NodeState(int num_peers){
    GUID=0;
    N=0;
    max_N=num_peers;
  }
      
  
  bool ring_completed(){
    if (N == max_N){
        return true; 
      }else{
        return false;
      }
  };
 
 
  string getPort(){
    if(available_ports.first<available_ports.second){
       available_ports.first++;    
    }
    return intToS(available_ports.first);
  }
 
 void nodeJoin(zframe_t* Identity,string _ip) {
    zframe_t* id_frame = zframe_dup(Identity);
    peer p(id_frame,_ip,getPort());
    peers.push_back(p);
    free(Identity);
 }

 void sendRMsg(zsock_t* channel, zframe_t* to, vector<string> parts) {
      zmsg_t* msg = zmsg_new();
      zframe_t* dto = zframe_dup(to);
      zmsg_prepend(msg, &dto);
      for (const string& s : parts) {
        zmsg_addstr(msg, s.c_str());
      }
      zmsg_send(&msg, channel);
 }

 void sendRMsg(zsock_t* channel,string action, peer p, vector<PairIpSk> parts) {
      zmsg_t* msg = zmsg_new();
      zframe_t* dto = zframe_dup(p.getZframe());
      zmsg_prepend(msg, &dto);
      zmsg_addstr(msg,action.c_str());
      
      string my_url=p.getIp()+":"+p.getPort();
      zmsg_addstr(msg,my_url.c_str());
      
      for (PairIpSk &frm : parts) {
          cout<<"parts to string"<<endl;
          cout<<frm.first<<endl;
          cout<<frm.second<<endl;
          string socket= frm.first+":"+frm.second;
          zmsg_addstr(msg, socket.c_str());
	  }
	  
      zmsg_send(&msg, channel);
 }


 void createRing(zsock_t* channel){
        for (int i = 0; i < max_N; i++){
            if (i == 0){
                peers[i].setNewSocket(peers[1].getIp(),peers[1].getPort());//setNewFrame(peers[1].getIp(),peers[1].getZframe());
                peers[i].setNewSocket(peers[max_N-1].getIp(),peers[max_N-1].getPort());
            }else if (i == max_N-1){
                peers[i].setNewSocket(peers[i-1].getIp(),peers[i-1].getPort());
                peers[i].setNewSocket(peers[0].getIp(),peers[0].getPort());
            }else{
                peers[i].setNewSocket(peers[i+1].getIp(),peers[i+1].getPort());
                peers[i].setNewSocket(peers[i-1].getIp(),peers[i-1].getPort());  
            }
        
            
          
	      cout<<"----------" << endl;
	      peers[i].sendMsg(channel,{"Sus actualizaciones estan a punto de ser enviadas"});
	      cout << "Tabla: "<< i << endl;
	      vector<PairIpSk> table_to_send=peers[i].getSockets();
          
         for(auto m : table_to_send){
                //zframe_print(m.second, "id:");
                cout <<"IP: "<< m.first<<"port:"<< m.second<< endl;   
            }

          //zframe_print(peers[i].getZframe().second,"EL PIROBO ESE");
          sendRMsg(channel,"tableSent",peers[i],table_to_send);
          sendRMsg(channel,peers[i].getZframe(),{"nodeCount",intToS(max_N)});
     }
 }
 

  void meetingPeers(zsock_t* channel,zframe_t * identity, string _ip){
    if (!ring_completed()) {
 
      cout<<  N <<"Hola, usted esta a punto de ser agregado"<<endl;
      sendRMsg(channel, identity, {"Hola, usted esta a punto de ser agregado"});
      
      nodeJoin(identity,_ip);
      N++; 
      if(ring_completed()){
         createRing(channel);
      }
    } else {
      cout << "NO MORE PEERS SUPORRTED" << endl;
      sendRMsg(channel, identity, {"sorry we are full"});
    }
  }


};



int handler(zloop_t*, zsock_t* channel, void* _node) {
  NodeState *node = reinterpret_cast<NodeState*>(_node);
  zmsg_t* msg = zmsg_recv(channel);
  zmsg_print(msg);

  zframe_t* identity = zmsg_pop(msg);
  
  zframe_t* active = zframe_dup(identity);
  zframe_t* action = zmsg_pop(msg);

 
  if (zframe_streq(action, "join")) {
     string ip = zmsg_popstr(msg);
     cout<<ip<<endl;
     node->meetingPeers(channel,identity,ip);
  }
}                  


int main(int argc, char** argv) {

    int num_peers=atoi(argv[1]);

    zsock_t *routerBoot = zsock_new(ZMQ_ROUTER);
    zsock_bind(routerBoot, "tcp://*:5555");
    
    cout<<endl<<"::::::::::::::::serverBoot is Runnig: port:5555::::::::::::::::"<<endl;
    
    NodeState *node = new NodeState(num_peers);

    zloop_t *loop = zloop_new();
    zloop_reader(loop,routerBoot,&handler,node);
    zloop_start(loop);
    //zloop_timer_end(loop,10);
    zsock_destroy(&routerBoot);
 return 0;
}