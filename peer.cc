#include <czmq.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <map>
#include <unistd.h>
#include <unordered_map>
#include <time.h>


using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::pair;
using std::unordered_map;
using std::ifstream;
using std::stringstream;
using std::pair;

using sha1Client=unordered_map<string,unordered_map<string,int>>;
using idSearch=unordered_map<string,unordered_map<string,int>>;

vector<string> split(string str, char delimiter){
  vector<string> internal;
  std::stringstream ss(str); // Turn the string into a stream.
  string tok;
  
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  
  return internal;
}

string intToS(int n){
    std::ostringstream sin;
    sin << n;
    std::string val = sin.str();
    return val;
}

void toStringVector(vector<string> strings){
      string str="";
      for(auto i: strings){
         str+=i+" ";
      }
      cout<<str<<endl;
}

class SongData{
    private:
    string SHA1;
    sha1Client names;   //vector<nameSong, unordered_map<"ip:puerto", 0>>
    int numparts;
    vector<string> partsSong;
    

    public:
    SongData(string _SHA1,int _numparts){
        SHA1 = _SHA1;
        numparts = _numparts;
    }
    
    // "key1:val1/val2/val3;key2:val1/;"
    
   void insertNames(string str,string socketClientStr){
        vector<string>namesHash = split(str,'/');
        //toStringVector(namesHash);
        
        for(auto& o:namesHash){
           if(existName(o)){
              //cout<<o<<"  esta cación un nuevo client"<<socketClientStr<<endl;
              names.at(o).insert({socketClientStr,0}); //insert a new Client 
           }else{
             //cout<<"una nueva canción"<<endl;
             unordered_map<string,int>n;
             n.insert({socketClientStr,0});
             names.insert({o,n});    
           }
        }
    }
    
    bool existName(string strName){
        sha1Client::const_iterator got = names.find(strName);
         if ( got == names.end()){
            return false;
          }else{
            return true;
          }
    }
    
    /*
    *lo que esta función devuelve: 
    *nombre=client1/client2/client3/;nombre=client1/client2/;
    *Nota: Por cada SHA1 que se elige en filemanager,se imprime los diferentes nombres
    *que tiene. 
    */
    string matchesNameSong(string nameSong){ // //buscar coincidencias en las canciones
      sha1Client n;
      string str="";
      
      for(auto obj: names){// aca tiene que hacer la busqueda dentro los nombres
        string strMatch=obj.first;
        
        std::size_t pos = strMatch.find(nameSong);
        if(strMatch.size()>=pos){
            string song=strMatch+"=";
            
            unordered_map<string,int> tempSockets=obj.second;
            
            for(auto& socketClient: tempSockets){
                song+=socketClient.first+"/";
            }
            song+=";";
            str+=song;
            std::cout << "exist!!!"<< strMatch << std::endl;
        }
      
       }
     return str; 
    }
    
    int getNumParts(){
      return numparts;
    }
    
};

class fileManager{
    private:
    string pathFiles;
    string pathCatalogo;
    unordered_map <string,SongData> files; // SHA1, SONG
    
    public:
    //"key1:val1/val2/val3;key2:val1/;"
    //key1:val1/val2/val3   -> "key1" "val1/val2/val3"
    //key2:val1/
    fileManager(string _pathFiles, string _pathCatalogo){
      pathFiles=_pathFiles;//"./csha1/files"; 
      pathCatalogo=_pathCatalogo;//"/catalogo";
    }
    
     
    void splitingMsgHash(string strMSg, string socketClientStr){ //socketClient
        vector<string> hash = split(strMSg,';');
        
        toStringVector(hash);
        for(auto h: hash){
            vector<string> hashName=split(h,':');
            string hashSong=hashName[0];
            string strNames=hashName[1];
            int parts=stoi(hashName[2]);
            
            if(existSong(hashSong)){
              //cout<<"ya exisistia!"<<endl;
              files.at(hashSong).insertNames(strNames,socketClientStr);
            }else{
               //cout<<"no exisistia!"<<endl;
               SongData song(hashSong,parts);
               song.insertNames(strNames,socketClientStr);
               files.insert({hashSong,song});
            }
        }
    }
    
    bool existSong(string sha1){ //busca por sha1
        unordered_map<string,SongData>::const_iterator got = files.find (sha1);
         if ( got == files.end()){
            return false;
          }else{
            return true;
          }
    }
    
    /*
    * El resultado de esta función es:
    * SHA1[nombre=client1/client2/client3/;nombre=client1/client2/;[numparts}SHA2[nombre=client1/client2/client3/;nombre=client1/client2/;[numparts
    * Por cada SHA1 imprime el SHA1  imprime lo que viene de matchesNameSong(_nameSong) 
    * y le concatena al final el numero de partes
    * recordar que los client1-> son sockets(ip:puerto) Lo cambie para ser mas rápido
    */
    
    /*Toca validar que las respuestas no sean del mismo cliente que las esta pidiendo, 
    *toca comparar al client->'ip:puerto' de cada canción contra 'ip:puerto' 
    *del cliente que esta haciendo la busqueda.
    */
    string matchSong(string _nameSong){ //busca por sha1
      std::cout << "entra a matchSong" << std::endl;
      string result;
      cout<<"numero de files="<<files.size()<<endl;
       for(auto& obj: files){
           string str=obj.first+"[";
           str+=obj.second.matchesNameSong(_nameSong);
           str+="[";
           str+=intToS(obj.second.getNumParts());
           str+="}";
           result+=str;
       }
       cout << result <<endl;
       return result;
    }
    
};
 
class socketDealer{
    private:
        zframe_t* identity;
        vector<string> ip_and_port;
        void * node_dealer;
        zctx_t* context;
        zmsg_t* msg_temp;
        string someThing;
        
    public:
    int numNodesInRed;
    
        socketDealer(zmq_pollitem_t* &items,zctx_t* context,string socket_to_connect, int pos_items){
           ip_and_port=split(socket_to_connect,':');
           identity=nullptr;
           node_dealer = zsocket_new(context,ZMQ_DEALER);
           string url_str1="tcp://"+socket_to_connect;
           cout<<"Create Socket Dealer:"<<url_str1.c_str()<<endl;
           zsocket_connect(node_dealer, url_str1.c_str());
           items[pos_items].socket=node_dealer;
           items[pos_items].events = ZMQ_POLLIN;
           //sendMsg(node_dealer,{"join",set_ip[i]});
        }
        
        socketDealer(string socket_to_connect){
           ip_and_port=split(socket_to_connect,':');
           identity=nullptr;
           context = zctx_new();   
           node_dealer = zsocket_new(context,ZMQ_DEALER);
           string url_str="tcp://"+socket_to_connect;
           cout<<"Create Temporal Socket Dealer:"<<url_str.c_str()<<endl;
           zsocket_connect(node_dealer, url_str.c_str());
        }
   
    void sendMsg(vector<string> parts){
        zmsg_t* msg = zmsg_new();
        for (const string& s : parts) {
          zmsg_addstr(msg, s.c_str());
        }
        zmsg_send(&msg, node_dealer);
    }
    
    
    void sendMsg(string action, vector<string> parts){
        zmsg_t* msg = zmsg_new();
        zmsg_addstr(msg, action.c_str());
        for (const string& s : parts) {
          zmsg_addstr(msg, s.c_str());
        }
        zmsg_send(&msg, node_dealer);
    }
   
    void handlerSocketTemporal(){
      while(true) {
        zmsg_t* msg = zmsg_recv(node_dealer);
       	//zmsg_print(msg);

        zframe_t* action = zmsg_pop(msg);
        
       if (zframe_streq(action, "tableSent")) {
          msg_temp=zmsg_dup(msg);
           
        }else if(zframe_streq(action, "nodeCount")){
          numNodesInRed= std::stoi(zmsg_popstr(msg));
          break;   
        }
      }
      zctx_destroy(&context);
    }
    
    
    /*******************SETs*********************/
    void setIdentity(zframe_t* _identity){
        identity=zframe_dup(_identity);
    }
    
    void setSomething(string _something){
        someThing= _something;
    }
        
    /*******************GETs*********************/
    void getSomething(){
        std::cout <<"algo:" << someThing << std::endl;
    }
    
    void * getSocket(){
     return node_dealer;
    }
    
    string getPort(){
        return ip_and_port[1];
    }
    
    string getIp(){
        return ip_and_port[0];
    }
    
    zframe_t* getIdentity(){
       return zframe_dup(identity);
    }
    
    zmsg_t* getMsgSocketTemp(){
     return msg_temp;
    }
};

class client{
    private:
    socketDealer *socketClient;
    zframe_t* identity;
    string portRouter;
    string ipRouter;
    double status;
    int downloads;
    int uploads;
    
    public:
    client(string _ipNode,zframe_t* identity,string _ipRouter, string _portRouter){
        identity=identity;
        ipRouter=_ipRouter;
        portRouter=_portRouter;
        status=0;
        downloads=0;
        uploads=0;
        string _socket_to_connect=ipRouter+":"+portRouter;
        socketClient=new  socketDealer(_socket_to_connect);
    }
    
    
    void response(vector<string>parts){
        socketClient->sendMsg(parts);
    }
    
    void response(string action,vector<string>parts){
        socketClient->sendMsg(action,parts);
    }
    
    zframe_t* getIdentity(){
     return zframe_dup(identity);
    }
    
    void set_Identity( zframe_t* _identity){
        identity = _identity;
    }
    
    void incrementDownloads(){
        downloads++;
    }
    
    void incrementUploads(){
        uploads++;
    }
    
    void calculateSatus(){
      if(uploads-downloads>0){
        status= (uploads-downloads)/uploads;
      }else if(uploads-downloads==0){
        status=0.5;  
      }else if(uploads-downloads<0){
        status=0.0;  
      }
    }
    
    void setStatus(double _status){
     status=_status;
    }
    
    /********GETs**********/
    int getStatus(){
        return status;
    }
    
    string getSocketRouter_str(){
        return  ipRouter+":"+portRouter;
    }
}; 

class Node {
  private:
    unordered_map<string,client> clients;
    vector <socketDealer> nodes;
    vector<string> set_ip;
    int socket_num;
    string portRouter;
    string ipRouter;
    vector<char*> sha1_search_list;
    idSearch recordsQuestions; // idPregunta, "pregunta" -> idpregunta= "ZFRAME"+"sec/dia/mes/año"
    
    int numNodesInRed;
    
    fileManager* manager;
    
   void* node_router;
   zmq_pollitem_t* items;
   zctx_t* context;
    
  public:
   
    Node(string _ipRouter, zmsg_t* _msg,int _numNodesInRed){
        ipRouter=_ipRouter;
        numNodesInRed=_numNodesInRed;
        frame_to_string(_msg);
        context=zctx_new();
        items=(zmq_pollitem_t*) malloc((socket_num)*sizeof(zmq_pollitem_t));//crea el vector de items dinamicamente
        manager= new fileManager("./csha1/files","/catalogo");
        createSocketRouter();
        createSocketsDealers();
        handler();
        
        zctx_destroy(&context);
   }
  
    void frame_to_string(zmsg_t* msg){
      vector<string> result;
      socket_num=zmsg_size(msg);
      
      while(zmsg_size(msg)!=0){
          string socket=zmsg_popstr(msg);
          result.push_back(socket);
        }
        
      set_ip = result;
    }
    
    void createSocketRouter(){
        vector<string> str=split(set_ip[0],':');
        portRouter=str[1];
        node_router = zsocket_new(context,ZMQ_ROUTER);
        string url_router="tcp://*:"+portRouter;
        zsocket_bind(node_router, url_router.c_str());
        items[0].socket=node_router;
        items[0].events = ZMQ_POLLIN;
        cout<<endl<<"::::::::::: zocketRouter in: "<<url_router<<":::::::::::::::"<<endl;
    }

  
    void createSocketsDealers(){
       for(int i=1; i<socket_num; i++){
           socketDealer socket(items,context,set_ip[i],i);
           socket.sendMsg({"node:join",portRouter});
           nodes.push_back(socket);
       }
    }
    
    socketDealer& findNode(string router_port){
        for(int i=0; i<socket_num-1; i++){
            if(nodes[i].getPort()==router_port){
                //zframe_print(nodes[i].getIdentity(),"print zframe:");
                return nodes[i];
            }
        }
    }
    
    socketDealer& findNode(zframe_t* identity){
        for(int i=0; i<socket_num-1; i++){
            if(zframe_eq (nodes[i].getIdentity(),identity)){
                //zframe_print(nodes[i].getIdentity(),"print zframe:");
                return nodes[i];// socketsDealers
            }
        }
    }
    
    bool existClient(zframe_t* _identity){
        string identityHex=zframe_strhex (_identity);
        unordered_map<string,client>::const_iterator got = clients.find (identityHex);
        
         if ( got == clients.end() )
            return false;
          else{
            return true;
          }
    }
    
    bool existRecordSearch(string idSearch){
        idSearch::const_iterator got = recordsQuestions.find (idSearch);
        
        if ( got == recordsQuestions.end() ){
         //cout << "Consulta ya se hizo !!!!!"<<endl;
            return false;
        }else{
            return true;
        } 
    }
    
    
    bool existNodeinQuestion(string idSearch,string hex_identity){
        
         unordered_map<string, int> matches=recordsQuestions.at(idSearch);
         unordered_map<string,int>::const_iterator got = matches.find (hex_identity);
             
             if(got != matches.end()){
                //cout << "COLAPSO!!!!!"<<endl; // ya todos le habian respondido
                return true;
             }else{
                return false;
             }
    }
    
    
    string getIdSearch(string identity){
      time_t rawtime;
      struct tm * timeinfo;
      char buffer [80];
    
      time (&rawtime);
      timeinfo = localtime (&rawtime);
    
      strftime (buffer,80,"%H%M%S%d%m%y",timeinfo);
      string recordTime(buffer);
      string strRecord=identity+recordTime;
      return strRecord;
    }
    
    /****************************ACTIONS**********************************/
    void actionjoinNode(zframe_t* identity,zmsg_t* msg_e){
         string router_port = zmsg_popstr(msg_e);
         findNode(router_port).setIdentity(identity);
         findNode(zframe_dup(identity)).sendMsg({"ok"});
    }
    
    void actionjoinClient(zframe_t* _identity,zmsg_t* _msg_e){
         string identityHex=zframe_strhex (_identity);

         string _ipRouter=zmsg_popstr(_msg_e);
         string _portRouter=zmsg_popstr(_msg_e);
         client newClient(ipRouter, _identity,_ipRouter,_portRouter);
         newClient.response("ok",{intToS(numNodesInRed)});
         pair<string,client> pairClient(identityHex,newClient);
         clients.insert(pairClient);
    }
    

    void recieve_hash(zframe_t* _identity,zmsg_t* _msg_e ){
        string identityHex=zframe_strhex (_identity);
        string all_nameHash = zmsg_popstr(_msg_e);
        string socketClient=clients.at(identityHex).getSocketRouter_str();
        manager->splitingMsgHash(all_nameHash,socketClient);
    }
   
   void actionSearchSong(zframe_t* _identity,zmsg_t* _msg_e){ //esto lo inicia el cliente 
        string identityHex=zframe_strhex (_identity);
        string search=zmsg_popstr(_msg_e);
        string idSearch=getIdSearch(identityHex);
        cout<<"idSearch:"<<idSearch<<endl;
        recordsQuestions.insert({idSearch,{}});//
        
        initBfS(identityHex,idSearch,search);
        //cout<<recordsQuestions.size()<<endl;
        string result = manager->matchSong(search);
        clients.at(identityHex).response("search_result",{portRouter,idSearch,result});
   }
   
   void initBfS(string identityHex,string _idSearch,string _search){
    string socketRouter=clients.at(identityHex).getSocketRouter_str();// formato->ip:puerto
     
    for(auto& adjacentNode: nodes){
        adjacentNode.sendMsg({"node:search",socketRouter,_idSearch,_search});
        string idAdjacentNode=zframe_strhex (adjacentNode.getIdentity());
        
        unordered_map<string,int> idAdjNode;
        idAdjNode.insert({idAdjacentNode,0});
        recordsQuestions.insert({_idSearch,idAdjNode});
    }
   }
   
   void BFS(zframe_t* _identity,zmsg_t* _msg_e){
       string identityHex=zframe_strhex (_identity);// identificador del nodo que me pregunto
       string socketRouterClient=zmsg_popstr(_msg_e); // a quien le tengo que responder
       string idSearch=zmsg_popstr(_msg_e);// identificador de la busqueda
       string search=zmsg_popstr(_msg_e);
       
       
        if(!existRecordSearch(idSearch)){
            //cout<<"aun no ha pasado"<<endl;
            socketDealer socketTemp(socketRouterClient);
            
            string result = manager->matchSong(search);// matches devuelve las conicidencias en un string
            //cout<< result<<endl;
            socketTemp.sendMsg("search_result",{portRouter,idSearch,result});//////
            
            unordered_map<string,int> idAdjNode;
            recordsQuestions.insert({idSearch,idAdjNode});
            recordsQuestions.at(idSearch).insert({identityHex,0});
            
            for(auto& adjacentNode: nodes){
                zframe_t * idAdjIdentity=adjacentNode.getIdentity();
                string idAdjIdentityHex=zframe_strhex(idAdjIdentity);
                
                if(!existNodeinQuestion(idSearch,idAdjIdentityHex)){
                    recordsQuestions.at(idSearch).insert({idAdjIdentityHex,0});
                    
                    findNode(idAdjIdentity).sendMsg({"node:search",socketRouterClient,idSearch,search});
                }
            }
        }else{
           //cout<<"ya paso por aca no la propage"<<endl;
        }
       
   }
   
   /**********************************************************************/
   
   void handler(){
        while(zmq_poll(items, socket_num, -1)>0){
    
            if (items[0].revents & ZMQ_POLLIN){
                zmsg_t* msg_e = zmsg_recv(node_router);
                zmsg_print(msg_e);
                zframe_t* identity = zmsg_pop(msg_e);
                zframe_t* action = zmsg_pop(msg_e);
                
                if (zframe_streq(action, "node:join")) {
                   actionjoinNode(identity,msg_e);
                }
                else if (zframe_streq(action, "client:join")) {
                    cout<<"clientConnect"<<endl;
                    actionjoinClient(zframe_dup(identity),msg_e);
                }
                else if(zframe_streq(action, "client:SendHash")){
                    recieve_hash(zframe_dup(identity),msg_e);
                }
                else if (zframe_streq(action, "client:search")) {
                    cout<<"Client Search"<<endl;
                    actionSearchSong(zframe_dup(identity),msg_e);
                }else if(zframe_streq(action, "node:search")){
                    cout<<"Node Search"<<endl;
                    BFS(zframe_dup(identity),msg_e);
                }
            }
        }
    }
   
};


int main(int argc, char** argv) {
    
    string myIp(argv[1]);

    socketDealer socketTemp("localhost:5555");//conexión temporal al peerBoot
    socketTemp.sendMsg({"join",myIp});
    socketTemp.handlerSocketTemporal();
    
    int _numNodesInRed=socketTemp.numNodesInRed;
    zmsg_t* msg= zmsg_dup(socketTemp.getMsgSocketTemp());
    std::cout << _numNodesInRed << std::endl;
    
    Node nodeState(myIp,msg,_numNodesInRed);
    return 0;
}
