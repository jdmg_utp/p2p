#include <czmq.h>
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <glob.h>
#include <unordered_map>
#include <thread>
#include <math.h>
#include <stdio.h>


using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;
using std::unordered_map;
using std::ifstream;
using std::stringstream;
using std::pair;
using std::thread;

using idSearch=unordered_map<string,int>; // idSearch, int numero de respuestas de los nodos
using sha1Client=unordered_map<string,unordered_map<string,vector<string>>>;

//"sha1:nombre=client1/client1 /client1/;nombre=client1/client1 /client1 /:numeropartes", "sha1:nombre:clients/ / / /:numero","sha1:nombre:clients/ / / /:numero","sha1:nombre:clients/ / / /:numero"

vector<string> split(string str, char delimiter) {
  vector<string> internal;
  std::stringstream ss(str); // Turn the string into a stream.
  string tok;
  
  while(getline(ss, tok, delimiter)) {
    internal.push_back(tok);
  }
  
  return internal;
}

/*
vector<string> globVector(const string& pattern){
    glob_t glob_result;
    glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> files;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        files.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
} 
*/

string intToS(int n){
    std::ostringstream sin;
    sin << n;
    std::string val = sin.str();
    return val;
}

class resultQuestion{
    private:
    unordered_map<string,pair<string,int>>matches;//name,SHA1
    string currentSearch; 
    
    public:
    sha1Client current;
    
    
    resultQuestion(string _currentSearch){
      currentSearch= _currentSearch;    
    }
    
    void setCurrent(sha1Client  _current){
      current=_current;
    }
    
    void insertNewName(string nameSong, string sha1Song, int numparts){
        matches.insert({nameSong,{sha1Song,numparts}});
    }
    
    void collectResultNames(){
    }
    
    void toStringMatches(){
       int i=1;
       std::cout << "Result Search:" << std::endl;
       if(matches.size()>0){
           for(auto m: matches){
               pair<string, int> sha1Parts=m.second;
               cout<<"-["<<i<<"]-"<<m.first<<" --> sha1:"<<sha1Parts.first<<"--> numparts: "<<sha1Parts.second<<endl;
               i++;
           }
       }else{
          std::cout << "(!) anything matching" << std::endl; 
       }
    }
    
    bool existSHA1Current(string sha1){
        sha1Client::const_iterator got = current.find (sha1);
        
        if ( got == current.end() ){
            return false;
        }else{
            return true;
        } 
    }
    
    bool existNameCurrent(unordered_map<string,vector<string>> structure ,string nameSong){
        unordered_map<string,vector<string>>::const_iterator got = structure.find(nameSong);
        
        if ( got == structure.end() ){
            return false;
        }else{
            return true;
        } 
    }


    void insertClients(string sha1, string nameSong, int numParts,vector<string> clients){
       unordered_map<string,vector<string>> temp;
        
       if(!existSHA1Current(sha1)){
        current.insert({sha1,temp});
       }else{
        // cout<<"ya existe sha1"<<endl;
       }
       
       if(!existNameCurrent(current.at(sha1),nameSong)){
         //cout<<"no existe canción"<<endl;
         current.at(sha1).insert({nameSong,clients});
       }else{
         //cout<<"ya existe canción"<<endl;
         for(auto clientSocket: clients){
            current.at(sha1).at(nameSong).push_back(clientSocket);
         } 
       }
       
       insertNewName(nameSong,sha1,numParts);
    }
    
    vector<string> getMatchByPosMap(string _numIndex){
       int numIndex=stoi(_numIndex);
       vector<string> match;
       int i=1;
       string nameSong;
       for(auto m: matches){
           if(i==numIndex){
             match.push_back(m.first);
             pair<string, int> sha1Num= m.second;
             match.push_back(sha1Num.first);
             match.push_back(intToS(sha1Num.second));
            return match;
           }
        i++;
       }
       return  match;
    }
    
    vector<string> getClientsInSong(string _numIndex){
      vector<string> match=getMatchByPosMap(_numIndex);
      string nameSong=match[0];
      unordered_map<string,vector<string>> structure;
      for(auto sha1: current){
         if(existNameCurrent(current.at(sha1.first),nameSong)){
             structure=current.at(sha1.first);
             return structure.at(nameSong);
         }
      } 
    }
    
    void toStringPeers(string _numIndex){
        vector<string> peers=getClientsInSong(_numIndex);
        vector<string> match=getMatchByPosMap(_numIndex);
        cout<<":::::::::::::::::: PEERS by: "<< match[0]<< "::::::::::::::::::"<<endl;
        for(auto peer:peers){
            cout <<"[-]"<< peer <<endl;
        }
    }
    
    void toStringCurrentSearch(){
      if(currentSearch==""){
          std::cout << "nothing sought" << std::endl;
      }else{
           std::cout << "current Search:"<< currentSearch << std::endl;
      }
    }
};

class SongData{
    private:
    string SHA1;
    vector<string> names;
    int numparts;
    vector<string> partsSong;
    bool Complete;

    public:
    SongData(string _SHA1,int _numparts){
        SHA1 = _SHA1;
        numparts = _numparts;
        Complete=true;
    }
    
    SongData(string _SHA1,string nameSong,int _numparts){
        SHA1 = _SHA1;
        numparts = _numparts;
        names.push_back(nameSong);
        Complete=false;
    }
    
    bool existName(string name){
      for(auto i: names){
        if(i==name){
            return true; 
         }
      }
      return false;
    }
    
    string toStringsNames(){
      string str="";
      for(auto i: names){
         str+=i+"/";
      }
      return str;
    }
    
    void insertName(string newName){
      if(!existName(newName)){
        names.push_back(newName);  
      }
    }
    
    void addPart(string part){
       partsSong.push_back(part); 
    }
    
    int getNumParts(){
        return numparts;
    }
    
    bool isComplete(){
       if(partsSong.size()==numparts){
          Complete=true;
       }else{
          Complete=false;
       }
      return Complete;   
    }
    
    void newPart(string namePart,int numpart){
       string n=intToS(numpart);
       if(numpart<10){
         n="0"+intToS(numpart);
       }
       partsSong.push_back(namePart+n); //SHA1 + parte -> 12wdkafdauihs03
    }
};

class fileManager{
    private:
    unordered_map <string,SongData> files; // SHA1, SONG
    int number_of_uploads;
    
    public:
    string pathFiles;
    string pathCatalogo;
    
    fileManager(string _pathFiles, string _pathCatalogo){
      pathFiles=_pathFiles;//"./csha1/files"; 
      pathCatalogo=_pathCatalogo;//"/catalogo";
      number_of_uploads = 0;
    }
    
    bool existSong(string sha1){
        unordered_map<string,SongData>::const_iterator got = files.find (sha1);
         if ( got == files.end()){
            return false;
          }else{
            return true;
          }
    }
    
    void loadFiles(){
      system("sh sha1.sh");
      cout<<"loading Files..."<<endl;
      sleep(2);
      system("clear");
    }
    
    void loadStruct(){
        loadFiles();
        ifstream file(pathFiles+pathCatalogo);
         
        if(file){
            stringstream buffer;
            buffer << file.rdbuf();
            file.close();
            string name,hash,tam;
            
            while(buffer>>name>>hash>>tam){
            //cout<<name<<":"<<hash<<":"<<tam<<endl;
                if(existSong(hash)){
                   files.at(hash).insertName(name);
                }else{
                   SongData s(hash,stoi(tam)); 
                   s.insertName(name);
                   files.insert({hash,s});
                }
                number_of_uploads++;
            }
            std::cout <<number_of_uploads<< " files loaded!";;
        }else{
          cout<<"no loaded Files (!)"<<endl;
        }
    }
    
    
    string toStringNameFiles(){ 
       string str="";
       for(auto& o : files){
           string key=o.first;
           string value=o.second.toStringsNames();
           string parts= intToS(o.second.getNumParts());
           str+=key+":"+value+":"+parts+";";
       }
       return str;
    }
    
    void insertNewSong(string sha1,string nameSong,int numparts){
     if(existSong(sha1)){
        files.at(sha1).insertName(nameSong);
     }else{
         SongData newSong(sha1,nameSong,numparts);
         files.insert({sha1,newSong});
     }
    }
    
    SongData &getSong(string sha1){
        return files.at(sha1);
    }

};



class socketDealer{
    private:
        zframe_t* identity;
        vector<string> ip_and_port;
        void * node_dealer;
        zctx_t* context;
        zmsg_t* msg_temp;
        string someThing;
        
    public:
        socketDealer(zmq_pollitem_t* &items,zctx_t* context,string socket_to_connect, int pos_items){
           ip_and_port=split(socket_to_connect,':');
           identity=nullptr;
           node_dealer = zsocket_new(context,ZMQ_DEALER);
           string url_str1="tcp://"+socket_to_connect;
           cout<<"Create Socket Dealer:"<<url_str1.c_str()<<endl;
           zsocket_connect(node_dealer, url_str1.c_str());
           items[pos_items].socket=node_dealer;
           items[pos_items].events = ZMQ_POLLIN;
           //sendMsg(node_dealer,{"join",set_ip[i]});
        }
        
        socketDealer(string socket_to_connect){
           ip_and_port=split(socket_to_connect,':');
           identity=nullptr;
           context = zctx_new();   
           node_dealer = zsocket_new(context,ZMQ_DEALER);
           string url_str="tcp://"+socket_to_connect;
           cout<<"Create Temporal Socket Dealer:"<<url_str.c_str()<<endl;
           zsocket_connect(node_dealer, url_str.c_str());
        }
   
    void sendMsg(vector<string> parts){
        zmsg_t* msg = zmsg_new();
        for (const string& s : parts) {
          zmsg_addstr(msg, s.c_str());
        }
        zmsg_send(&msg, node_dealer);
    }
   
    void handlerSocketTemporal(){
      while(true) {
        zmsg_t* msg = zmsg_recv(node_dealer);
       	//zmsg_print(msg);

        zframe_t* action = zmsg_pop(msg);
        
       if (zframe_streq(action, "tableSent")) {
          msg_temp=zmsg_dup(msg);

          break;  
        }
      }
      zctx_destroy(&context);
    }
    
    
    void sendMsg_Chunk(vector<string> parts,zchunk_t* chunk){
        zframe_t *frameChunk = zframe_new(zchunk_data(chunk),zchunk_size(chunk));
        zmsg_t* msg = zmsg_new();
        for (const string& s : parts) {
            zmsg_addstr(msg, s.c_str());
        }
        zmsg_append(msg,&frameChunk);
        zmsg_send(&msg, node_dealer);
    }  
    
    
    /*******************SETs*********************/
    void setIdentity(zframe_t* _identity){
        identity=zframe_dup(_identity);
    }
    
    void setSomething(string _something){
        someThing= _something;
    }
        
    /*******************GETs*********************/
    void getSomething(){
        std::cout <<"algo:" << someThing << std::endl;
    }
    
    void * getSocket(){
     return node_dealer;
    }
    
    string getPort(){
        return ip_and_port[1];
    }
    
    string getIp(){
        return ip_and_port[0];
    }
    
    zframe_t* getIdentity(){
       return identity;
    }
    
    zmsg_t* getMsgSocketTemp(){
     return msg_temp;
    }
};



class Node {
  private:
    socketDealer *socket;
    int socket_num;
    resultQuestion *resultsSearch;
    fileManager *manager;
    //Son los atributos para hacer la conexión con un dealer hacia el router de un peer del anillo
    string portRouterConnect;
    string ipRouterConnect;
    
    //para crear mi router 
    string my_ip;
    string my_port;
    double status;
    int downloads;
    int uploads;
    //numero de server nodes que hay en el anillo
    
    idSearch recordsResults;
    
  public:
   int numNodesServersInRed;
   void* node_router;
   zmq_pollitem_t* items;
   zctx_t* context;
   
    Node(string _my_ip,string _my_port ,string _ipRouterConnect, string _portRouterConnect,fileManager *_manager){
        
        my_ip = _my_ip;
        my_port = _my_port;
        status = 0.0f;
        downloads=0;
        uploads = 0;
        manager=_manager;
        ipRouterConnect=_ipRouterConnect;
        portRouterConnect=_portRouterConnect;
        
        socket_num=2;
        context=zctx_new();
        items=(zmq_pollitem_t*) malloc((socket_num)*sizeof(zmq_pollitem_t));//crea el vector de items dinamicamente
       
        createSocketRouter();
        createSocketsDealer();
   }
  
    void createSocketRouter(){
        node_router = zsocket_new(context,ZMQ_ROUTER);
        string url_router="tcp://*:"+my_port;
        zsocket_bind(node_router, url_router.c_str());
        items[0].socket=node_router;
        items[0].events = ZMQ_POLLIN;
    }

    void createSocketsDealer(){
       string url=ipRouterConnect+":"+portRouterConnect;
       socket= new socketDealer(items,context,url.c_str(),1);
       //std::cout << "sendMSg: join" << std::endl;
       socket->sendMsg({"client:join",my_ip,my_port});
    }

    void incrementDownloads(){
        downloads++;
    }
    
    void incrementUploads(){
        uploads++;
    }
    
    void calculateSatus(){
      if(uploads-downloads>0){
        status= (uploads-downloads)/uploads;
      }else if(uploads-downloads==0){
        status=0.5;  
      }else if(uploads-downloads<0){
        status=0.0;  
      }
      //cout<<"your status: "<<(status*100)<<"%"<<endl;
    }
    
    bool existRecordSearch(string idSearch){
        idSearch::const_iterator got = recordsResults.find (idSearch);
        
        if ( got == recordsResults.end() ){
            return false;
        }else{
            return true;
        } 
    }
    
    bool existSHA1currentResult(string sha1){
        sha1Client::const_iterator got = resultsSearch->current.find (sha1);
        
        if ( got == resultsSearch->current.end() ){
            return false;
        }else{
            return true;
        } 
    }
    
    bool existNamecurrentResult(unordered_map<string,vector<string>> structure ,string sha1){
        unordered_map<string,vector<string>>::const_iterator got = structure.find(sha1);
        
        if ( got == structure.end() ){
            return false;
        }else{
            return true;
        } 
    }

    //* SHA1[nombre=client1/client2/client3/;nombre=client1/client2/;[numparts}SHA2:nombre=client1/client2/client3/;nombre=client1/client2/;:numparts
    
    void recolect_pieces(string result){
          
         
         if(result!=""){
             vector<string>parts_str=split(result,'}') ;
             for(auto  part: parts_str){
                vector<string> info_song=split(part,'[');
                string sha1=info_song[0];
                string namesSongs=info_song[1];
                int  numparts=stoi(info_song[2]);

                vector<string> songs=split(namesSongs,';');
                for(auto song:songs){
                    
                    vector<string> parts_songs=split(song,'=');
                    string nameSong=parts_songs[0];
                    string socketsClients=parts_songs[1];
                    vector<string> clients_availible = split(socketsClients, '/');
                    resultsSearch->insertClients(sha1,nameSong,numparts,clients_availible);
                }
             }
         }
    }

    vector<string> loadBalancing(int _numPeers,int _numParts){
       vector<string>parts;
       int numTotalPeers;
       
       if(status==0.0){
          numTotalPeers=1;
          //cout<<"es igual a 0 el estatus"<<endl;
        }else{
          //cout << "status es diferentes de->0" <<endl;
          numTotalPeers=ceil(_numPeers*status); 
        }
    
        //std::cout <<"numero segun el status atendiendo la petición"<<numTotalPeers << std::endl;
        int quantityByPeer=ceil(_numParts/numTotalPeers);
        int numParts=_numParts;
        int max=0;
        
       for(int j=0; j<_numPeers; j++){
            //parts.push_back("ok");
            max+=quantityByPeer;
            numParts-=quantityByPeer;
            
            if(numParts>=0){
               parts.push_back(intToS(max-quantityByPeer)+"-"+intToS(max)); 
            }else{
                parts.push_back("");
            }
            //segun el numero de clientes por cada peer 
            //asignarseos segun el numero de parets  
        }
       
       
       return parts;
    }
    
   /****************************ACTIONS**********************************/
    
    void setCurrentResults(zframe_t* _identity,zmsg_t* _msg_e){
       string portResult=zmsg_popstr(_msg_e);
       string idSearch=zmsg_popstr(_msg_e);
       string result=zmsg_popstr(_msg_e);
       //std::cout << result << std::endl;
       //recordsResults
       
      if(!existRecordSearch(idSearch)){
         vector<string>results;
         results.push_back(result);
         recordsResults.insert({idSearch,1});
      }else{
          recordsResults.at(idSearch)+=1;
          int sizeResult=recordsResults.at(idSearch);
          //cout<<sizeResult<<endl;
          if(sizeResult==numNodesServersInRed){
           // cout<<"ya se lleno:"<<sizeResult<<endl;
          }

      }
      
      recolect_pieces(result);
    }
    
    void sendMsg(vector<string>parts){
        socket->sendMsg(parts);
    } 
    
    void search(string str){
       socket->sendMsg({"client:search",str});
       resultsSearch=new resultQuestion(str);
    }
   
    void initDonwload(string indexSong){
       incrementDownloads();    
       calculateSatus();
       
       
       vector<string>nameSha1Parts=resultsSearch->getMatchByPosMap(indexSong);
       vector<string> vect= resultsSearch->getClientsInSong(indexSong);
       string mysocket=my_ip+":"+my_port;
       
       int numPeers=vect.size()-1;
       
       string nameSong=nameSha1Parts[0];
       string sha1=nameSha1Parts[1];
       int numparts=stoi(nameSha1Parts[2]);
       vector<string> parts=loadBalancing(numPeers,numparts);
       //
       //numpeers=3
       //numpartes=3
       //tiene una reputación donde le devolvieron 2 clientes.
       //0:1-3;1:4-6;2:;
       
       /*
       if(!manager->existSong(nameSha1Parts[1])){
           string mysocket=my_ip+":"+my_port;
           cout<<"mi socket"<<mysocket<<endl;
           
       }else{
         cout<<"you have "<<nameSha1Parts[0]<<" in your files."<<endl;
       }
      */
        int i=0;
        for(auto socketPeer:vect){
           if(socketPeer!=mysocket && parts[i]!=""){
              socketDealer *socket= new socketDealer(socketPeer); //socketDealer(string ipNode, string socket_to_connect){
              socket->sendMsg({"peer:getSong",mysocket,sha1,nameSong,parts[i],intToS(numparts)});
               i++;
            }
        }
        
    }
    
    void initUpload(zframe_t* _socketPeer,zmsg_t* _msg_e){

       string socketPeer=zmsg_popstr(_msg_e);
       string sha1Song=zmsg_popstr(_msg_e);
       string nameSong=zmsg_popstr(_msg_e);
       string parts=zmsg_popstr(_msg_e);
       string numpTotal = zmsg_popstr(_msg_e);
       incrementUploads();
       calculateSatus();
       
       vector<string>numparts=split(parts,'-');
       
       cout<<"---------------------Get request Song-----------------------------"<<endl;
       cout<<"by: "<<socketPeer<<" sha1:"<<sha1Song<<" name:"<<nameSong<<" total parts: "<<numpTotal<<endl;
       cout<<"Parts: from-> "<<numparts[0]<<" to->"<<numparts[1]<<endl<<endl;
       cout<<"------------------------------------------------------------------"<<endl;
       
       socketDealer socketToPeer(socketPeer);
       
       for(int i= atoi(numparts[0].c_str()); i<atoi(numparts[1].c_str()); i++){
           if(i<atoi(numpTotal.c_str())){
               string n=intToS(i);
               if(i<10){
                 n="0"+intToS(i);
               }
               std::cout << "n: "<<n << std::endl;
               string path=manager->pathFiles+'/'+sha1Song+'/'+sha1Song+n;
               //std::cout << path << std::endl;
               zchunk_t *chunk =zchunk_slurp(path.c_str(),0);
               //zchunk_t *chunk = zchunk_slurp("./files/a7977ac4766dad5b697b9a4320be9fed928ee5f6/a7977ac4766dad5b697b9a4320be9fed928ee5f601",0);
               
               socketToPeer.sendMsg_Chunk({"peer:partsSong",sha1Song,nameSong,intToS(i),numpTotal},chunk);//sendMsg_Chunk(vector<string> parts,zchunk_t* chunck){ 
           }
       }
      //Lo vamos hacer mejor por partes originales no por el chuck compuesto.
    }
    
    void incomingPartsSong(zframe_t* _socketPeer,zmsg_t* _msg_e){
       string sha1Song=zmsg_popstr(_msg_e);
       string nameSong=zmsg_popstr(_msg_e);
       string partSong=zmsg_popstr(_msg_e);
       int numParts=atoi(zmsg_popstr(_msg_e));
       
       zframe_t* filePart =zmsg_pop(_msg_e);// es el que esta compuesto y contatenado desde un numero de una parte hasta donde le corresponde
       zchunk_t *chunk = zchunk_new(zframe_data(filePart),zframe_size(filePart));
       
       //string path=manager->pathFiles+"/"+sha1Song;
       string path="test/"+sha1Song;
       string f=sha1Song+partSong[0]+partSong[1];
       
       zfile_t* download = zfile_new(path.c_str(),f.c_str());
       zfile_output(download);
       
       zfile_write(download,chunk,0);
       
       zfile_close(download);
       
       //string path=manager->pathFiles+'/'+sha1Song+'/'+sha1Song+"*";
       //string path=manager->pathFiles+'/'+sha1Song+'/'+sha1Song+"*";
       //vector<string> files = globVector(path);
       
       
       //std::cout<<"tamaño :"<< files.size() << std::endl;
       /*
       if(files.size()==numParts){
         
       }
       */
       //sengChuck({sha1,name,partFrom,num},chuck);

    }
    
    /****************************SETs**********************************/
    
    void setStatus(double _status){
     status=_status;
    }

     void setUploads_number(double _uploads){
     uploads =_uploads;
    }
    
     void setDownloads_number(double _downloads){
     downloads=_downloads;
    }

    /****************************GETs**********************************/
    
    resultQuestion *getResultsSearch(){
       return resultsSearch;
    } 
    
    int getSocketNum(){
        return socket_num;
    }
    
    int getStatus(){
        return status*100;
    }
 
    string getMySocket(){
        return my_ip+":"+my_port;
    }
   
    int get_downloads_number(){
        return downloads;
    }
    
    int get_uploads_number(){
        return uploads;
    }
    
};


   void handler(Node* client){
       while(zmq_poll(client->items, client->getSocketNum(), -1)>0){
           
           if (client->items[0].revents & ZMQ_POLLIN){
                zmsg_t* msg_e = zmsg_recv(client->node_router);
                //zmsg_print(msg_e);
                zframe_t* identity = zmsg_pop(msg_e);
                zframe_t* action = zmsg_pop(msg_e);
                
                if (zframe_streq(action, "ok")) {
                    //cout<<"Connected"<<endl;
                    client->numNodesServersInRed = atoi(zmsg_popstr(msg_e));
                    //std::cout <<"Numero de Nodos en la red "<< client->numNodesServersInRed << std::endl;
                }else if(zframe_streq(action, "search_result")){
                    //cout<<"llego result"<<endl;
                    client->setCurrentResults(zframe_dup(identity),msg_e);
                }else if(zframe_streq(action, "peer:getSong")){
                    client->initUpload(zframe_dup(identity),msg_e);
                }else if(zframe_streq(action, "peer:partsSong")){
                    cout<<"peer:partsSong"<<endl;
                    zmsg_print(msg_e);
                    client->incomingPartsSong(zframe_dup(identity),msg_e);
                }
           }
           
       }
       zctx_destroy(&client->context);
    }
    
    void viewInfo(Node *client,resultQuestion *result){
        system("clear");
        std::cout<<"[-] listening for: "<<client->getMySocket();
        std::cout << "--[-] status:"<<client->getStatus();
        std::cout << "--[-] uploads:"<<client->get_uploads_number();
        cout<<" --[-] downloads:"<<client->get_downloads_number();
    }
    
    
    void viewMenu(Node *client,resultQuestion *result){
        viewInfo(client,result);
        cout<<endl<<"************************************"<<endl;
        cout<<"******** Menu Options:  *******"<<endl;
        cout<<"1 - Search"<<endl;
        cout<<"2 - "<<endl;
        cout<<"3 - "<<endl;
        cout<<"0 - Quit\n "<<endl;
        cout<<"************************************"<<endl;
        cout<<"************************************"<<endl;
    }
    
    void viewMenuSearch(Node *client,resultQuestion *result){
        viewInfo(client,result);
        cout<<endl<<"************************************"<<endl;
        cout<<"Search"<<endl;
        cout<<"******** Menu Search Options:  *******"<<endl;
        cout<<"1 - View Peers "<<endl;
        cout<<"2 - donwload Song"<<endl;
        cout<<"************************************"<<endl;
        result->toStringCurrentSearch();
        cout<<"************************************"<<endl;
    }
    
    void menu(Node *client){
      int op=1;
      string cad;
      char caracter;
      resultQuestion *result=new resultQuestion("") ;
      while(op!=0){
        viewMenu(client,result);
        cout<<"Insert option menu:";
        cin>>op;
        if(op==1){//search
          int op2=1;
          string cad;
          cout<<"Insert a name:";
          cin>>cad;
          client->search(cad);
          
          std::cout << "searching..." << std::endl;
          sleep(1);
          //system("clear");
          result=client->getResultsSearch();
          result->toStringMatches();
          getchar();
          getchar();
            while(op2!=0){
                 viewMenuSearch(client,result);
                 result->toStringMatches();
                 cout<<"************************************"<<endl;
                 cout<<"Insert option menu:";
                 int op3=0;
                 cin>>op3;
                 if(op3==1){
                        string cad3;
                        cout<<endl<<"{*} insert a number of song: ";
                        cin>>cad3;
                        result->toStringPeers(cad3); 
                 }
                 if(op3==2){
                        string cad4;
                        cout<<endl<<"{*} insert a number of song: ";
                        cin>>cad4;
                        client->initDonwload(cad4);  
                 }
                 std::cout << "exit Menu Search? yes->0 / no->1" << std::endl;
                 cin>>op2;
            }
          //printf("\n El caracter que ingreso es %c", caracter);
          //system("clear");
        }
      }
    }




int main(int argc, char **argv){
  
  
  string my_ip(argv[1]);
  string my_port(argv[2]);
  
  string ipRouterConnect(argv[3]);
  string portRouterConnect(argv[4]);

  
  fileManager manager("./files","/catalogo");
  manager.loadStruct();
  
  
  Node client(my_ip,my_port, ipRouterConnect,portRouterConnect,&manager);
  //Node client("localhost","5556", "localhost","1025");
  
  client.sendMsg({"client:SendHash",manager.toStringNameFiles()});
  
  thread h1(handler,&client);
  thread h2(menu,&client);
  h1.join();
  h2.join();


 return 0;
}
