CC=g++ -std=c++11
CA= -std=c++11

ZMQ=/usr/local
#ZMQ=/home/utp/zmq

ZMQ_INCS=$(ZMQ)/include
ZMQ_LIBS=$(ZMQ)/lib


all: peer server client fileS 

clean:
	rm peer server client fileS

peer: peer.cc
	$(CC) -I$(ZMQ_INCS) -L$(ZMQ_LIBS) -o peer peer.cc -lczmq -lzmq -lcrypto
	
fileS: fileSearch.cc
	$(CC) -I$(ZMQ_INCS) -L$(ZMQ_LIBS) -o fileS fileSearch.cc

client:  clientConnect.cc
	$(CC) -I$(ZMQ_INCS) -L$(ZMQ_LIBS) -o client clientConnect.cc -lczmq -lzmq

server: serverBoot.cc
	$(CC) -I$(ZMQ_INCS) -L$(ZMQ_LIBS) -o server serverBoot.cc -lczmq -lzmq
	
	