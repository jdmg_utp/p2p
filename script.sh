clear
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH

if test "$1" = "runserver"
then
    ./server 3
fi

if test "$1" = "peer" 
 then
    ./peer localhost
fi

if test "$1" = "client" 
 then
    ./client "$2" "$3" "$4" "$5"
    #./client
 fi

if test "$1" = "make" 
 then
    make peer client -j4
fi

