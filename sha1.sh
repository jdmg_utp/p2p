#!/bin/bash

#calcuate sha1sum value of each file and split files
cd files
#rm *[^.mp3]
cat /dev/null > catalogo
for i in *.mp3;
do
    sha1=$(sha1sum $i)
    set -- $sha1
    mkdir $sha1
    name=$1
    ruta=$(pwd)
    cd $sha1
    n_parts=$(split -b 512k --numeric-suffixes=0 --verbose $ruta/$i $name | wc -l )
    echo $n_parts > $name.parts
    cd ..
    echo $i $name $n_parts >> catalogo
done
#$PWD
#http://stackoverflow.com/questions/918886/how-do-i-split-a-string-on-a-delimiter-in-bash
#n_parts=$(split -b 512k --numeric-suffixes=1 --verbose /home/ubuntu/workspace/files/$i $name | wc -l )