#include <glob.h>
#include <vector>
#include <string>
#include <iostream>

// unordered_map::bucket
#include <unordered_map>


using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::unordered_map;
using std::pair;

using namespace std;


vector<string> globVector(const string& pattern){
    glob_t glob_result;
    glob(pattern.c_str(),GLOB_TILDE,NULL,&glob_result);
    vector<string> files;
    for(unsigned int i=0;i<glob_result.gl_pathc;++i){
        files.push_back(string(glob_result.gl_pathv[i]));
    }
    globfree(&glob_result);
    return files;
}

class client{
    private:
    string name;
    string age;

    public:
    string someThing;
    client(string _name, string _age){
        name=_name;
        age=_age;
    }
    
    string getName(){
        return name;
    }
    
    void setSomething(string s){
        someThing=s;
    }
};

using clientPair=pair<string,client>;

int main(){
    /*
        //vector<string> files = globVector("./*");
        vector<string> files = globVector("music/*norrea*.mp3");
        
        for (auto o:files)
            {
                cout <<o<<endl;
            }
            
            
            
  client c("john","21");
     
  unordered_map<string,client> mymap = {{"12",c}};
     
  unordered_map<string,client>::const_iterator got = mymap.find ("12");
  
  
  mymap.at("12").setSomething("algo");
  cout<<mymap.at("12").someThing<<endl;
  
  */
  
  std::string str="We think in generalities, but we live in details.";
                                           // (quoting Alfred N. Whitehead)

  std::string str2 = str.substr (12,12);   // "generalities"

  std::size_t pos = str.find("q");      // position of "live" in str

 if(str.size()>=pos){
     std::cout << "existe!!!" << std::endl;
 }else{
    std::cout << "no existe!!!" << std::endl; 
 }
  //std::string str3 = str.substr (pos);     // get from "live" to the end

  std::cout << pos <<' '<< '\n';
  
 /*
  if ( got == mymap.end() )
    std::cout << "not found";
  else{
   client s= got->second;
   s.setSomething();
   cout<<s.getName()<<endl;
   // std::cout << got.first << " is " << got.second
  }
  std::cout << std::endl;
  */
   /*
      for (auto& x: mymap) {
        std::cout << "Element [" << x.first << ":" << x.second << "]";
        std::cout << " is in bucket #" << mymap.bucket (x.first) << std::endl;
      }
   */  
      
    return 0;
}